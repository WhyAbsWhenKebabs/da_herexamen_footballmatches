package footballMatches.domain;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-10T23:21:48")
@StaticMetamodel(FootballMatch.class)
public class FootballMatch_ { 

    public static volatile SingularAttribute<FootballMatch, String> awayTeam;
    public static volatile SingularAttribute<FootballMatch, Date> dateOfMatch;
    public static volatile SingularAttribute<FootballMatch, String> lastConfrontation;
    public static volatile SingularAttribute<FootballMatch, Long> footballMatchID;
    public static volatile SingularAttribute<FootballMatch, String> homeTeam;
    public static volatile SingularAttribute<FootballMatch, String> odd;

}