package footballMatches.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Tom
 */
@Table(name = "t_FootballMatches_FootballMatch")
@Entity
public class FootballMatch implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "footballMatch_id", updatable = false, nullable = false)
    private Long footballMatchID;
    
    @Column(name = "away_team", nullable = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Pattern(regexp = "[a-zA-Z]{1,30}")
    private String awayTeam;
    
    @Column(name = "home_team", nullable = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Pattern(regexp = "[a-zA-Z]{1,30}")
    private String homeTeam;    
    
    @Column(name = "date_of_match", nullable = false)
    @Temporal(TemporalType.DATE)
    @NotNull
    @Future
    private Date dateOfMatch;
    
    @Column(name = "odd", nullable = false)
    @NotNull
    @Pattern(regexp = "[0-9]{1,2}:[0-9]{1,2}")
    private String odd;
    
    @Column(name = "last_confrontation", nullable = true)
    @Pattern(regexp = "[0-9]{1,2}-[0-9]{0,2}")
    private String lastConfrontation;

    public FootballMatch() {
        
    }

    public FootballMatch(String awayTeam, String homeTeam, String odd, String lastConfrontation, Date dateOfMatch) {
        this.setAwayTeam(awayTeam);
        this.setHomeTeam(homeTeam);
        this.setOdd(odd);
        this.setLastConfrontation(lastConfrontation);
        this.setDateOfMatch(dateOfMatch);
    }

    public Long getFootballMatchID() {
        return footballMatchID;
    }

    public void setFootballMatchID(Long footballMatchID) {
        this.footballMatchID = footballMatchID;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getOdd() {
        return odd;
    }

    public void setOdd(String odd) {
        this.odd = odd;
    }

    public String getLastConfrontation() {
        return lastConfrontation;
    }

    public void setLastConfrontation(String lastConfrontation) {
        this.lastConfrontation = lastConfrontation;
    }

    public Date getDateOfMatch() {
        return dateOfMatch;
    }

    public void setDateOfMatch(Date dateOfMatch) {
        this.dateOfMatch = dateOfMatch;
    }
    
    @Override
    public String toString(){
        return this.getHomeTeam() + " - " + this.getAwayTeam() + "  odd: " + this.getOdd() + "   last confrontation: " + this.getLastConfrontation();
    }
    
}
