package footballMatches.domain.restService.cafe;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author Tom
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cafe {
    
    private String name;
    
    private Address address;
    
    private String favouriteTeam;

    public Cafe() {
        
    }  
    
    public Cafe(String name, Address address, String favouriteTeam) {
        this.setName(name);
        this.setAddress(address);
        this.setFavouriteTeam(favouriteTeam);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getFavouriteTeam() {
        return favouriteTeam;
    }

    public void setFavouriteTeam(String favouriteTeam) {
        this.favouriteTeam = favouriteTeam;
    }    
    
    @Override
    public String toString(){
        return this.getName() + " with address: " + this.getAddress() + " and favourite team: " + this.getFavouriteTeam();
    }
    
}
