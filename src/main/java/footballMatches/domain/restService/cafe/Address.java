package footballMatches.domain.restService.cafe;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author Tom
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {
    
    private String street;
    
    private String city;
    
    private Integer number;
    
    private Integer postcode;

    public Address() {
        
    }

    public Address(String street, String city, Integer number, Integer postcode) {
        this.setStreet(street);
        this.setCity(city);
        this.setNumber(number);
        this.setPostcode(postcode);
    }    

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getPostcode() {
        return postcode;
    }

    public void setPostcode(Integer postcode) {
        this.postcode = postcode;
    }
    
    @Override
    public String toString(){
        return this.getCity() + " " + this.getPostcode() + " " + this.getStreet() + " " + this.getNumber();
    }
}   
