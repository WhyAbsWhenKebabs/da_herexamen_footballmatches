package footballMatches.db;

import footballMatches.domain.FootballMatch;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

/**
 *
 * @author Tom
 */
@RequestScoped
public class FootballMatchDB implements FootballMatchRepository{
    
    @PersistenceContext(unitName = "FootballMatchesPU")
    private EntityManager manager;
    
    @Resource
    private Validator validator;

    public FootballMatchDB() {

    }

    public EntityManager getManager() {
        return manager;
    }

    public void setManager(EntityManager manager) {
        this.manager = manager;
    }   

    public Validator getValidator() {
        return validator;
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }
    
    private void checkConstraints(Object object) throws ValidationException{
        Set<ConstraintViolation<Object>> constraintsAdress = this.getValidator().validate(object);
        if(!constraintsAdress.isEmpty()){
            String fullErrorConstraint = "";
            for (ConstraintViolation<Object> constraint : constraintsAdress) {
                fullErrorConstraint = fullErrorConstraint + constraint.getMessage() + "\n";
            }
            throw new ValidationException(fullErrorConstraint);
        }
    }
    
    @Transactional
    @Override
    public long addFootballMatch(FootballMatch footballMatch) throws ValidationException, DBException {
        this.checkConstraints(footballMatch);
        this.getManager().persist(footballMatch);
        return footballMatch.getFootballMatchID();
    }

    @Override
    public FootballMatch getFootballMatch(long footballMatchID) throws ValidationException, DBException {
        if(footballMatchID < 0 || footballMatchID > Long.MAX_VALUE){
            throw new ValidationException("Invalid footballMatchID given");
        }
        FootballMatch footballMatch = this.getManager().find(FootballMatch.class, footballMatchID);
        if(footballMatch == null){
            throw new DBException("No footballMatch exist with given footballMatchID");
        }
        return footballMatch;
    }

    @Override
    public List<FootballMatch> getFootballMatches() {
        String queryString = "SELECT f FROM FootballMatch f";            
        Query query = this.getManager().createQuery(queryString);
        return query.getResultList();
    }
    
    @Transactional
    @Override
    public void deleteFootballMatch(long footballMatchID) throws ValidationException, DBException {
        FootballMatch footballMatch = this.getFootballMatch(footballMatchID);
        manager.remove(footballMatch);
    }
    
    @Transactional
    @Override
    public void updateFootballMatch(FootballMatch footballMatch) throws ValidationException, DBException {
        this.getFootballMatch(footballMatch.getFootballMatchID()); // check if given footballMatch exists
        this.checkConstraints(footballMatch);
        this.manager.merge(footballMatch);
    }
}
