package footballMatches.db;

import java.io.Serializable;
import javax.ejb.ApplicationException;

/**
 *
 * Application Exception - example when no object was found in database
 *  no rollback is needed for this exception - like default aplication exception
 * 
 * @author Tom
 */
@ApplicationException(rollback = false)
public class DBException extends Exception implements Serializable{
    
    private static final long serialVersionUID = 1L;

    public DBException(){
	super();
    }
	
    public DBException(String message){
	super(message);
    }
	
    public DBException(Exception e){
	super(e);
    }
	
    public DBException(String message, Exception e){
	super(message, e);
    }
}
