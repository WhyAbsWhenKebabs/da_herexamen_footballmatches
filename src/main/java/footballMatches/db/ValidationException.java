package footballMatches.db;

import java.io.Serializable;
import javax.ejb.ApplicationException;

/**
 *
 * Application Exception - example when invalid business logic was given
 *  rollback is needed - we want to avoid for sure commiting invalid data
 * 
 * @author Tom
 */
@ApplicationException(rollback = true)
public class ValidationException extends Exception implements Serializable{
    
    private static final long serialVersionUID = 1L;

    public ValidationException(){
	super();
    }
	
    public ValidationException(String message){
	super(message);
    }
	
    public ValidationException(Exception e){
	super(e);
    }
	
    public ValidationException(String message, Exception e){
	super(message, e);
    }
    
}
