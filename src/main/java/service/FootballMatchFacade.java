package service;

import footballMatches.db.DBException;
import footballMatches.db.ValidationException;
import footballMatches.domain.FootballMatch;
import java.util.List;
import javax.ejb.Local;
/**
 *
 * @author Tom
 */
@Local
public interface FootballMatchFacade {
    
    long addFootballMatch(FootballMatch footballMatch) throws ValidationException, DBException;
    FootballMatch getFootballMatch(long footballMatchID) throws ValidationException, DBException;
    List<FootballMatch> getFootballMatches();
    void deleteFootballMatch(long footballMatchID) throws ValidationException, DBException;
    void updateFootballMatch(FootballMatch footballMatch) throws ValidationException, DBException;
}
