package service;

import footballMatches.db.DBException;
import footballMatches.db.FootballMatchRepository;
import footballMatches.db.ValidationException;
import footballMatches.domain.FootballMatch;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

/**
 *
 * @author Tom
 */
@Dependent
@Stateless
public class FootballMatchService implements FootballMatchFacade{
    
    @Inject
    FootballMatchRepository footballMatchRepository;
    
    public FootballMatchService() {
        
    }   
    
    @Override
    public long addFootballMatch(FootballMatch footballMatch) throws ValidationException, DBException {
        return this.footballMatchRepository.addFootballMatch(footballMatch);
    }

    @Override
    public FootballMatch getFootballMatch(long footballMatchID) throws ValidationException, DBException {
        return this.footballMatchRepository.getFootballMatch(footballMatchID);
    }

    @Override
    public List<FootballMatch> getFootballMatches() {
        return this.footballMatchRepository.getFootballMatches();
    }

    @Override
    public void deleteFootballMatch(long footballMatchID) throws ValidationException, DBException {
        this.footballMatchRepository.deleteFootballMatch(footballMatchID);
    }

    @Override
    public void updateFootballMatch(FootballMatch footballMatch) throws ValidationException, DBException {
        this.footballMatchRepository.updateFootballMatch(footballMatch);
    }
    
}
